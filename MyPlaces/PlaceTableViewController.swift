//
//  TableViewController.swift
//  MyPlaces
//
//  Created by Kamil Popczyk on 01.01.2018.
//  Copyright © 2018 Kamil Popczyk. All rights reserved.
//

import UIKit
import os.log

class TableViewController: UITableViewController {
    
    // MARK: Properties
    
    var places = [Place]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = editButtonItem
        loadSamplePlaces()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return places.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "PlaceTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PlaceTableViewCell else {
            fatalError("The dequeued cell is not an instance of PlaceTableViewCell.")
        }
        let place = places[indexPath.row]
        
        cell.nameLabel.text = place.name
        cell.photoImageView.image = place.photo
        cell.ratingControl.rating = place.rating
        
        return cell
    }
    
    

     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }

    
    
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
        places.remove(at: indexPath.row)
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }

    
    
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let place = places[fromIndexPath.row]
        places.remove(at: fromIndexPath.row)
        places.insert(place, at: destinationIndexPath.row)
     }
    
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? ""){
        case "AddPlace":
            os_log("Dodawanie nowej miejscowki", log: OSLog.default, type: .debug)
        case "ShowDetail":
            guard let placeDetailViewController = segue.destination as? PlaceViewController
                else{
                     fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedPlaceCell = sender as? PlaceTableViewCell else {
                fatalError("Unexpected sender: \(sender)")
            }
            guard let indexPath = tableView.indexPath(for: selectedPlaceCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            let selectedPlace = places[indexPath.row]
            placeDetailViewController.place = selectedPlace
        default:
            fatalError("Unexpected Segue Identifier; \(segue.identifier)")
        }
     }
 

    
    //MARK: Actions
    @IBAction func unwindToPlaceList(sender: UIStoryboardSegue){
        if let sourceViewController = sender.source as?
            PlaceViewController, let place = sourceViewController.place {
              if let selectedIndexPath = tableView.indexPathForSelectedRow {
                places[selectedIndexPath.row] = place
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            }
              else {
                let newIndexPath = IndexPath(row: places.count, section: 0)
                places.append(place)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
        }
    }
    
    //MARK: Private Methods
    
    private func loadSamplePlaces() {
        let photo1 = UIImage(named: "place1")
        let photo2 = UIImage(named: "place2")
        let photo3 = UIImage(named: "place3")
        
        guard let place1 = Place(name: "Wrocław", photo: photo1, rating: 5) else
        {
            fatalError("Unable to instantiante place1")
        }
        guard let place2 = Place(name: "Warszawa", photo: photo2, rating: 4) else
        {
            fatalError("Unable to instantiante place2")
        }
        guard let place3 = Place(name: "Paryż", photo: photo3, rating: 3) else
        {
            fatalError("Unable to instantiante place3")
        }
        
        places += [place1, place2, place3]
    }
    
}

