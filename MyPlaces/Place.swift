//
//  Place.swift
//  MyPlaces
//
//  Created by Kamil Popczyk on 31.12.2017.
//  Copyright © 2017 Kamil Popczyk. All rights reserved.
//

import UIKit

class Place {
    // MARK: Properties
    
    var name: String
    var photo: UIImage? // opcjonalnie
    var rating: Int
    
    // MARK: Initialization
    
    init?(name: String, photo: UIImage?, rating: Int) {

      //  if name.isEmpty || rating < 0 {
      //      return nil
      //  }
    
        guard !name.isEmpty else {
            return nil
        }
        guard (rating >= 0) && (rating <= 5) else{
            return nil
        }
        
        self.name = name
        self.photo = photo
        self.rating = rating
    }
}
