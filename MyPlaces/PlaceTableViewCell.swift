//
//  PlaceTableViewCell.swift
//  MyPlaces
//
//  Created by Kamil Popczyk on 31.12.2017.
//  Copyright © 2017 Kamil Popczyk. All rights reserved.
//

import UIKit

class PlaceTableViewCell: UITableViewCell {

    // MARK: Properties
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var ratingControl: RatingControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
