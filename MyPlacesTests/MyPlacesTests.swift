//
//  MyPlacesTests.swift
//  MyPlacesTests
//
//  Created by Kamil Popczyk on 22.12.2017.
//  Copyright © 2017 Kamil Popczyk. All rights reserved.
//

import XCTest
@testable import MyPlaces

class MyPlacesTests: XCTestCase {
    //MARK: Place class tests
    
    func testPlaceInitialization(){
        // 0 rating
        let zeroRatingPlace = Place.init(name: "Zero", photo: nil, rating: 0)
        XCTAssertNotNil(zeroRatingPlace)
        
        // max rating
        let positiveRatingPlace = Place.init(name: "Positive", photo: nil, rating: 5)
        XCTAssertNotNil(positiveRatingPlace)
    }
    func testPlaceInitializationFails() {
        let negativeRatingPlace = Place.init(name: "Negative", photo: nil, rating: -1)
        XCTAssertNil(negativeRatingPlace)
        let largeRatingPlace = Place.init(name: "Large", photo: nil, rating: 6)
        XCTAssertNil(largeRatingPlace)
        let emptyStringPlace = Place.init(name: "", photo: nil, rating: 0)
        XCTAssertNil(emptyStringPlace)
    }
}
